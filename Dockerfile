FROM python:3.9

COPY ./app/ /app
WORKDIR /app

# RUN apt-get update ; apt-get -y install libmariadb-dev

RUN pip install --no-cache-dir -r ./requirements.txt
# RUN bash ./init_db.sh

ENTRYPOINT ["bash", "./start.sh"]

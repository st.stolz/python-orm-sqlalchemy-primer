# Python ORM SqlAlchemy Primer

## Start ohne vscode

Um auf die Python CLI zu gelangen folgende Befehle verwenden:

```
docker-compose up
docker-compose run --entrypoint python app
```

## Start mit vscode

1. Remote Containers Extension installieren
2. "Open Folder in Container" in vscode aufrufen
3. Auf der CLI `python` ausführen
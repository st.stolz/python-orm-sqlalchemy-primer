from sqlalchemy import create_engine
engine = create_engine("mysql://root:123@mysql/college",echo = True)

from sqlalchemy import Table, Column, Integer, String, MetaData
meta = MetaData()

students = Table(
   'students', meta, 
   Column('id', Integer, primary_key = True), 
   Column('name', String(255)), 
   Column('lastname', String(255)), 
)

meta.create_all(engine)

conn = engine.connect()
ins = students.insert().values(name = 'Seppl', lastname = 'Huber')
result = conn.execute(ins)